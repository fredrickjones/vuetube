import Vue from 'vue';
import Router from 'vue-router';
import store from './store.js';

// views
import Home from './views/Home.vue';
import Search from './views/Search.vue';
import Favorites from './views/Favorites.vue';
import NotFound from './views/NotFound.vue'

Vue.use(Router);

const router = new Router({
  mode: 'history',
  routes: [
    { path: '/', name: 'home', component: Home },
    { path: '/search', name: 'search', component: Search, meta: { requiresAuth: true } },
    { path: '/favorites', name: 'favorites', component: Favorites, meta: { requiresAuth: true } },
    { path: '*', name: 'not-found', component: NotFound }
  ]
});

router.beforeEach((to, from, next) => {
  if (to.matched.some(record => record.meta.requiresAuth)) {
    if (!store.state.user.isAuthed) {
      next({
        path: '/'//,
        // query: {
        //   redirect: to.fullPath
        // }
      });
    } else {
      next();
    }
  } else {
    next();
  }
});

export default router;
