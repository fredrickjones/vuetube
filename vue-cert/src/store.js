import Vue from 'vue';
import Vuex from 'vuex';
import Axios from 'axios';
import API_KEYS from '@/../APIKeys';

import router from './router.js';

Vue.use(Vuex);

const store = new Vuex.Store({
  state: {
    user: {
      email: '',
      password: '',
      isAuthed: false
    },
    selectedVideo: {},
    videos: [],
    videoApi: {
      baseUrl: 'https://www.googleapis.com/youtube/v3/videos?',
      part: 'snippet',
      id: '',
      key: API_KEYS.YOU_TUBE
    }
  },
  getters: {
    getUser: state => state.user//,
    // getVideos: state => state.videos,
    // getVideo: state => state.selectedVideo
  },
  mutations: {
    INIT_STORE_STATE(state) {
      // check if store exists
      if(localStorage.getItem('store')) {
        // replace the state object with the stored one
        this.replaceState(
          Object.assign(state, JSON.parse(localStorage.getItem('store')))
        );
      }
    },
    USER_LOGIN(state, payload) {
      state.user.email = payload.email;
      state.user.password = payload.password;
      state.user.isAuthed = true;
    },
    USER_LOGOUT(state) {
      state.user.email = '';
      state.user.password = '';
      state.user.isAuthed = false;
    },
    YOU_TUBE_SEARCH(state, payload) {
      state.videos = [];
      // console.log(payload);
      Axios.get(payload).then(res => {
        state.videos = res.data.items;
        store.dispatch('setSelectedVideo', res.data.items[0].id.videoId);
      });
    },
    SET_SELECTED_VIDEO(state, payload) {
      Axios.get(payload).then(res => {
        state.selectedVideo = res.data.items[0];
      });
    }
  },
  actions: {
    userLogin(context, payload) {
      if (payload.email && payload.password) {
        setTimeout(() => {
          context.commit('USER_LOGIN', payload);
          router.push({ path: '/search'});
        }, 3000);
      }
    },
    userLogout(context) {
      if (context.user.isAuthed) {
        context.commit('USER_LOGOUT');
      }
    },
    youtubeSearch(context, payload) {
      context.commit('YOU_TUBE_SEARCH', payload);
    },
    setSelectedVideo(context, payload) {
      context.state.videoApi.id = payload;
      const { baseUrl, part, id, key } = context.state.videoApi;
      const apiUrl = `${baseUrl}part=${part}&id=${id}&key=${key}`;
      context.commit('SET_SELECTED_VIDEO', apiUrl);
    }
  }
});

store.subscribe((mutation, state) => {
  // storing the state object as a JSON string
  localStorage.setItem('store', JSON.stringify(state));
});

export default store;
